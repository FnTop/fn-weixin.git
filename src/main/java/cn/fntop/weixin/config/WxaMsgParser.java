package cn.fntop.weixin.config;

/**
 * 小程序消息解析
 *
 * @author fn
 */
public enum WxaMsgParser {
	/**
	 * json 、xml
	 */
	JSON, XML
}
