package cn.fntop.weixin.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.*;

/**
 * @author fn
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping
@Controller
public @interface FnMsg {
    /**
     * Alias for {@link RequestMapping#value}.
     *
     * @return {String[]}
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};
}
