package cn.fntop.weixin.config;

import com.jfinal.json.JacksonFactory;
import com.jfinal.kit.StrKit;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.weixin.sdk.cache.IAccessTokenCache;
import com.jfinal.weixin.sdk.utils.JsonUtils;
import com.jfinal.wxaapp.WxaConfig;
import com.jfinal.wxaapp.WxaConfigKit;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 微信应用配置
 *
 * @author fn
 */
@Configuration
@RequiredArgsConstructor
public class WeixinAppConfig implements SmartInitializingSingleton {
    private final FnConfig config;
    private final List<IAccessTokenCache> tokenProviders;
    private final List<WxConfigLoader> configProviders;

    @Override
    public void afterSingletonsInstantiated() {
        tokenProviders.sort(AnnotationAwareOrderComparator.INSTANCE);
        //通过@order(1) 通过order最大值获取哪个实现，默认1,即SpringAccessTokenCache
        ApiConfigKit.setAccessTokenCache(tokenProviders.get(tokenProviders.size() - 1));
        configProviders.sort(AnnotationAwareOrderComparator.INSTANCE);
        WxConfigLoader lastConfigLoader = configProviders.get(configProviders.size() - 1);
        List<ApiConfig> loadWxConfig = lastConfigLoader.loadWx();
        if (!CollectionUtils.isEmpty(loadWxConfig)) {
            List<ApiConfig> wxConfList = new ArrayList<>(loadWxConfig);
            for (ApiConfig conf : wxConfList) {
                ApiConfig config = new ApiConfig();
                if (StrKit.notBlank(conf.getAppId())) {
                    config.setAppId(conf.getAppId());
                }
                if (StrKit.notBlank(conf.getAppSecret())) {
                    config.setAppSecret(conf.getAppSecret());
                }
                if (StrKit.notBlank(conf.getToken())) {
                    config.setToken(conf.getToken());
                }
                if (StrKit.notBlank(conf.getEncodingAesKey())) {
                    config.setEncodingAesKey(conf.getEncodingAesKey());
                }
                config.setEncryptMessage(conf.isEncryptMessage());
                ApiConfigKit.putApiConfig(config);
            }

        }
        List<WxaConfig> loadWxaConfigs = lastConfigLoader.loadWxa();
        if (!CollectionUtils.isEmpty(loadWxaConfigs)) {
            List<WxaConfig> wxaConfList = new ArrayList<>(loadWxaConfigs);
            for (WxaConfig conf : wxaConfList) {
                WxaConfig config = new WxaConfig();
                if (StrKit.notBlank(conf.getAppId())) {
                    config.setAppId(conf.getAppId());
                }
                if (StrKit.notBlank(conf.getAppSecret())) {
                    config.setAppSecret(conf.getAppSecret());
                }
                if (StrKit.notBlank(conf.getToken())) {
                    config.setToken(conf.getToken());
                }
                if (StrKit.notBlank(conf.getEncodingAesKey())) {
                    config.setEncodingAesKey(conf.getEncodingAesKey());
                }
                config.setMessageEncrypt(conf.isMessageEncrypt());
                WxaConfigKit.setWxaConfig(config);
            }
        }
        boolean isDev = config.getDevMode();
        ApiConfigKit.setDevMode(isDev);
        if (WxaMsgParser.JSON == config.getWxaMsgParser()) {
            WxaConfigKit.useJsonMsgParser();
        }
        if ("jackson".equalsIgnoreCase(config.getJsonType())) {
            JsonUtils.setJsonFactory(JacksonFactory.me());
        }
    }

}
